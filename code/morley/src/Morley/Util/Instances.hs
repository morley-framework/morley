-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Missing instances from libraries.
module Morley.Util.Instances () where

import Data.Default (Default(..))

instance Default Natural where
  def = 0
