-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Conversions between tuples and list-like types.
module Morley.Util.TypeTuple
  ( RecFromTuple (..)
  ) where

import Morley.Util.TypeTuple.Class
import Morley.Util.TypeTuple.Instances ()
