# Tests: the cross-package test suite

## Purpose

This package is created to keep tests-only and is an umbrella for all the
tests involving multiple packages, causing a circular dependency otherwise.

The problem here is that [both cabal and stack do not handle circular dependencies good enough](https://gitlab.com/morley-framework/morley/-/issues/233#note_361426945);
the former fails to build the project altogether, the latter does build it but is rebuilding
one of the packages twice. As soon as the issue is resolved, this package won't be needed anymore.

## Inclusion criteria

Basically, it's only one: *when placing a test-suite into its home package would introduce circular dependencies between packages*.

## History

Quote from the [#758](https://gitlab.com/morley-framework/morley/-/issues/758):

<blockquote>

Long ago, in #233, the `cleveland` testing framework was created, putting all of our testing code in a single package.

One downside of doing that is that test suites of other libraries (e.g. `morley`) then had `cleveland` as a dependency
and sadly this means (at least currently) that compilation will be rejected due to cyclic dependencies
between **packages** (even tho there is none between `libraries` and `tests` **components**).

So, to introduce as little clutter as possible,
[it was decided](https://gitlab.com/morley-framework/morley/-/issues/233#note_361426945) to move those tests suites
under the `cleveland` package itself (e.g. `morley-test`).
Recently however [it was discussed](https://gitlab.com/morley-framework/morley/-/issues/755#note_840628411)
how this is un-intuitive and conceptually off.

In the end the agreement was that a better solution would be to instead:
- move all test suites that don't cover `cleveland` itself out of the `cleveland` package to a new one
  (e.g. a `tests` one)
- properly document why this new package exists and the criteria for adding a new test suite to it

</blockquote>
