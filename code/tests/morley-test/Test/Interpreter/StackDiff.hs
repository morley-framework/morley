-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Interpreter.StackDiff
  ( test_stack_diff
  ) where

import Prelude hiding (Const, EQ, GT, LT)

import Data.Coerce (coerce)
import Data.Constraint ((\\))
import Data.Default (Default(..), def)
import Data.Map qualified as Map
import Data.Some (Some(..), foldSome, mkSome)
import Data.Vinyl (Rec(..))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Michelson.Interpret
import Morley.Michelson.Runtime.Dummy
import Morley.Michelson.Typed
import Morley.Util.PeanoNatural

newtype Unique t = Unique { unUnique :: Natural }
  deriving stock Show
  deriving newtype (NFData, Eq, Ord)

instance Default (Unique t) where
  def = Unique 0

newtype UniqueSt = UniqueSt
  { usCounter :: Natural
  }

type EvalOpUnique = EvalOpT (State UniqueSt)

getNext :: State UniqueSt Natural
getNext = do
  n <- gets usCounter
  modify $ coerce @(Natural -> Natural) succ
  pure n

instance StkElMeta Unique (State UniqueSt) where
  mkStkElMeta _ _ = Unique <$> getNext

instance StkElMeta Unique EvalOpUnique where
  mkStkElMeta _ _ = Unique <$> lift getNext

newtype SomeStkEl = SomeStkEl { unSomeStkEl :: Some (StkEl Unique) }
  deriving newtype Eq

instance Ord SomeStkEl where
  compare = compare `on` foldSome (unUnique . seMeta) . unSomeStkEl

data StackDiff = StackDiff
  { sdAdded :: Map SomeStkEl Int
  , sdRemoved :: Map SomeStkEl Int
  , sdMoved :: [((Int, Int), SomeStkEl)]
  }

instance Default StackDiff where
  def = StackDiff def def def

data StackDiffSimple = StackDiffSimple
  { sdsAdded :: [(Int, SomeValue)]
  , sdsRemoved :: [(Int, SomeValue)]
  , sdsMoved :: [((Int, Int), SomeValue)]
  } deriving stock (Eq, Show)

instance Default StackDiffSimple where
  def = StackDiffSimple def def def

toSimple :: StackDiff -> StackDiffSimple
toSimple StackDiff{..} = StackDiffSimple
  { sdsMoved = sortWith fst $ fmap toValue <$> sdMoved
  , sdsAdded = sortWith fst $ fmap toValue . swap <$> toPairs sdAdded
  , sdsRemoved = sortWith fst $ fmap toValue . swap <$> toPairs sdRemoved
  }
  where
    toValue (SomeStkEl (Some (StkEl val))) = SomeValue val \\ valueTypeSanity val

diffStacks :: Rec (StkEl Unique) t1 -> Rec (StkEl Unique) t2 -> StackDiff
diffStacks from to =
  let lFrom = rToList [] from
      lTo = rToList [] to
  in diff (pred $ length lFrom) (pred $ length lTo) lFrom lTo def
  where
    rToList :: [SomeStkEl] -> Rec (StkEl Unique) xs -> [SomeStkEl]
    rToList acc RNil = acc
    rToList acc (x :& xs) = rToList (SomeStkEl (mkSome x) : acc) xs

    updateRemoved, updateAdded
      :: Int -> SomeStkEl -> StackDiff -> StackDiff
    updateRemoved currentDepth r sd@StackDiff{..}
      | Just newDepth <- Map.lookup r sdAdded
      = sd{sdMoved = ((currentDepth, newDepth), r) : sdMoved, sdAdded = Map.delete r sdAdded}
      | otherwise
      = sd{sdRemoved = Map.insert r currentDepth sdRemoved}

    updateAdded currentDepth a sd@StackDiff{..}
      | Just oldDepth <- Map.lookup a sdRemoved
      = sd{sdMoved = ((oldDepth, currentDepth), a) : sdMoved, sdRemoved = Map.delete a sdRemoved}
      | otherwise
      = sd{sdAdded = Map.insert a currentDepth sdAdded}

    diff :: Int -> Int -> [SomeStkEl] -> [SomeStkEl] -> StackDiff -> StackDiff
    diff _ _ [] [] = id
    diff depthX depthY (x : xs) [] =
      diff (pred depthX) depthY xs [] . updateRemoved depthX x
    diff depthX depthY [] (y : ys) =
      diff depthX (pred depthY) [] ys . updateAdded depthY y
    diff depthX depthY (x : xs) (y : ys) =
      diff (pred depthX) (pred depthY) xs ys . if x == y
        then id
        else updateAdded depthY y . updateRemoved depthX x

test_stack_diff :: [TestTree]
test_stack_diff =
  [ testCase "Test StackDiff" do
      testStackDiff (VNat 1 :& VNat 2 :& RNil) ADD @?= StackDiffSimple
        { sdsAdded = [(0, SomeValue (VNat 3))]
        , sdsRemoved = [(0, SomeValue (VNat 1)), (1, SomeValue (VNat 2))]
        , sdsMoved = []
        }
  , testCase "Test StackDiff when values are added/removed, but the stack doesn't change" do
      testStackDiff (VInt 0 :& VInt 2 :& RNil) (ADD :# PUSH (VInt 0)) @?= StackDiffSimple
        { sdsAdded = [(0, SomeValue (VInt 0)), (1, SomeValue (VInt 2))]
        , sdsRemoved = [(0, SomeValue (VInt 0)), (1, SomeValue (VInt 2))]
        , sdsMoved = []
        }
  , testCase "Test StackDiff when values are not added/removed at all" do
      testStackDiff (VInt 1 :& VInt 2 :& RNil) (PUSH (VInt 1) :# PUSH (VInt 1) :# ADD :# DROP) @?= def
  , testCase "Test StackDiff when values are moved" do
      testStackDiff (VInt 1 :& VInt 2 :& RNil) SWAP @?= StackDiffSimple
        { sdsAdded = []
        , sdsRemoved = []
        , sdsMoved = [((0, 1), SomeValue (VInt 1)), ((1, 0), SomeValue (VInt 2))]
        }
      testStackDiff longStack (DIG Two) @?= StackDiffSimple
        { sdsAdded = []
        , sdsRemoved = []
        , sdsMoved = [((0,1),SomeValue (VInt 1)),((1,2),SomeValue (VInt 2)),((2,0),SomeValue (VInt 3))]
        }
  , testCase "Test StackDiff when values are duped" do
      testStackDiff longStack DUP @?= StackDiffSimple
        { sdsAdded = [(0, SomeValue (VInt 1))]
        , sdsRemoved = []
        , sdsMoved = []
        }
      testStackDiff longStack (DUPN (Succ Two)) @?= StackDiffSimple
        { sdsAdded = [(0, SomeValue (VInt 3))]
        , sdsRemoved = []
        , sdsMoved = []
        }
  ]
  where
    longStack =
      VInt 1 :& VInt 2 :& VInt 3 :& VInt 4 :& VInt 5 :& VInt 6 :& VInt 7 :& VInt 8 :& VInt 9 :& RNil

testStackDiff :: Rec Value t -> Instr t t' -> StackDiffSimple
testStackDiff inputValueStack instr = do
  let env = dummyContractEnv
      ist = InterpreterState
        { isRemainingSteps = 9999999999
        , isBigMapCounter = 0
        , isGlobalCounter = 0
        }
      (inputStack, outputStack) = evaluatingState (UniqueSt 0) do
        inp <- mapToStkEl inputValueStack
        out <- unsafe . rslResult <$> runEvalOpT (runInstr instr inp) env ist
        pure (inp, out)
  toSimple $ diffStacks inputStack outputStack
