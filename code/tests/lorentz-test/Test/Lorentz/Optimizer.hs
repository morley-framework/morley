-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for optimization of Lorentz-specific instruction sequences.
module Test.Lorentz.Optimizer
  ( unit_eq0
  , unit_non
  , unit_stGet
  , unit_stUpdate
  ) where

import Lorentz ((#), (:->))
import Lorentz qualified as L
import Morley.Michelson.Optimizer (optimize)
import Morley.Michelson.Typed.Util (linearizeLeft)
import Test.HUnit (Assertion, (@?=))

-- | Check whether code on left hand side optimizes to the code on
-- the right hand side.
infix 1 ->?=
(->?=) :: i :-> o -> i :-> o -> Assertion
(L.iAnyCode -> pre) ->?= (L.iAnyCode -> post) =
  linearizeLeft (optimize pre) @?= linearizeLeft post

unit_eq0 :: Assertion
unit_eq0 =
  L.push @Integer 0 # L.eq ->?= L.eq0

unit_non :: Assertion
unit_non =
  L.non @Integer 0 ->?= L.nonZero

unit_stUpdate :: Assertion
unit_stUpdate =
  L.dip (L.if_ (L.push (Just ())) (L.push Nothing)) # L.stUpdate L.this ->?= update'
    where
      update' :: (Integer : Bool : Set Integer : s) :-> (Set Integer : s)
      update' = L.update

unit_stGet :: Assertion
unit_stGet =
  L.stGet L.this # L.isSome ->?= mem'
    where
      mem' :: (Integer : Set Integer : s) :-> (Bool : s)
      mem' = L.mem
