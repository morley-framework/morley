-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE NoApplicativeDo #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}
{-# OPTIONS_GHC -Wno-orphans #-}

-- | Contracts for tickets tests.
module Test.Lorentz.Tickets.Contracts
  ( atomicMissileLaunchContract

  , PermitTokensStorage (..)
  , PermitTokensParameter
  , permitTokensContract

  , allowancesContract
  ) where

import Prelude (Typeable)

import Lorentz
import Lorentz.Tickets

-- Errors
----------------------------------------------------------------------------

type instance ErrorArg "insufficient_tokens_permitted_by_ticket" =
  ("permitted" :! Natural, "spent" :! Natural)

instance CustomErrorHasDoc "insufficient_tokens_permitted_by_ticket" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Attempt to spend more tokens than permitted."

----------------------------------------------------------------------------
-- Contract with authorized action
----------------------------------------------------------------------------

atomicMissileLaunchContract
  :: (Typeable ticketer)
  => TAddress ticketer () -> Contract (Ticket Integer) Bool ()
atomicMissileLaunchContract ticketer = defaultContract $ do
  car; push ticketer
  authorizeAction validateCode
  push True; nil; pair
  where
    validateCode =
      push 12345 # eq # if_ nop (failUsing [mt|Bad code|])

----------------------------------------------------------------------------
-- Contract with authorized tokens spend from single address
----------------------------------------------------------------------------

data PermitTokensStorage = PermitTokensStorage
  { ptsPermittedTokens :: Maybe (STicket "tokens" ())
  , ptsAdmin :: Address
    -- ^ Who permits tokens spending
  } deriving stock (Generic)
    deriving anyclass (IsoValue, HasAnnotation, HasDupableGetters)

instance TypeHasDoc PermitTokensStorage where
  typeDocMdDescription = "Storage"

data PermitTokensParameter
  = PermitSpend Natural
  | PermitAllow (Ticket ())
  | PermitGet (Void_ () Natural)
  | PermitSetAdmin Address
  deriving stock (Generic)
  deriving anyclass (IsoValue)

instance ParameterHasEntrypoints PermitTokensParameter where
  type ParameterEntrypointsDerivation PermitTokensParameter = EpdPlain

-- | A contract that emulates simple allowance functionality, where
-- permission is provided via tickets.
permitTokensContract
  :: Contract PermitTokensParameter PermitTokensStorage ()
permitTokensContract = defaultContract $ do
  doc $ dStorage @PermitTokensStorage
  unpair; caseT
    ( #cPermitSpend /-> do
        setFieldOpen
          (subtractSTicket (failCustom #insufficient_tokens_permitted_by_ticket))
          #ptsPermittedTokens
    , #cPermitAllow /-> do
        toSTicket
        dipN @3 (getField #ptsAdmin); dig @3; verifyTicketer
        drop @()
        setFieldOpen addSTicket #ptsPermittedTokens
    , #cPermitGet /-> void_ do
        drop @()
        toField #ptsPermittedTokens
        sTicketAmount
    , #cPermitSetAdmin /-> do
        setField #ptsAdmin
        -- have to reset permissions according to the STicket's rules
        none; setField #ptsPermittedTokens
    )
  nil; pair

----------------------------------------------------------------------------
-- Contract with authorized tokens spend from many addresses
----------------------------------------------------------------------------

type AllowancesStorage =
  BigMap Address (STicket "allowances" ())

data AllowancesParameter
  = AllowancesSpend (Address, Natural)
  | AllowancesAllow (Ticket ())
  | AllowancesGet (Void_ Address Natural)
  deriving stock (Generic)
  deriving anyclass (IsoValue)

instance ParameterHasEntrypoints AllowancesParameter where
  type ParameterEntrypointsDerivation AllowancesParameter = EpdPlain

-- | A contract that emulates simple allowance functionality, where
-- permission is provided via tickets.
allowancesContract
  :: Contract AllowancesParameter AllowancesStorage ()
allowancesContract = defaultContract $ do
  unpair; caseT
    ( #cAllowancesSpend /-> do
        unpair @Address @Natural; swap
        dupN @2 @Address
        dip @Address $ do
          dip @Natural $ do
            dip none; getAndUpdate
          subtractSTicket (failCustom #insufficient_tokens_permitted_by_ticket)
        update
    , #cAllowancesAllow /-> do
        toSTicket; dip (drop @())
        dip swap
        stackType @[Address, AllowancesStorage, STicket _ _]
        dup; dip @Address $ do
          dip none; getAndUpdate
          dig @2
          addSTicket
        update
    , #cAllowancesGet /-> void_ do
        get; sTicketAmount
    )
  nil; pair
