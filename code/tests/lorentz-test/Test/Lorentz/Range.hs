-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Property tests for the 'Range' types.
module Test.Lorentz.Range
  ( test_range
  ) where

import Hedgehog (MonadTest, forAll, property, (===))
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)

import Lorentz
import Morley.Michelson.Interpret (MichelsonFailed(..), MichelsonFailureWithStack(..))
import Morley.Util.Named

test_range :: [TestTree]
test_range =
  [ testGroup "Range" $ testCommon @Range
  , testGroup "RangeIE" $ testCommon @RangeIE
  , testGroup "RangeEI" $ testCommon @RangeEI
  , testGroup "RangeEE" $ testCommon @RangeEE
  ----------------------------------
  , testProperty "mkRangeFor is isomorphic to mkRangeForSafe_" $ property do
      start :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      len :: Natural <- forAll $ Gen.integral $ Range.linearFrom 0 0 100_000
      mkRangeFor start (fromIntegral len) ===
        (mkRangeForSafe_ -$ (#start :! start) ::: (#length :! len))
  , testProperty "mkRangeForSafe_ can not construct empty ranges" $ property do
      start :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      len :: Natural <- forAll $ Gen.integral $ Range.linearFrom 0 0 100_000
      let range = (mkRangeForSafe_ -$ (#start :! start) ::: (#length :! len))
      isRangeEmpty range === False
  , testProperty "inRange checks whether a value is in range" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      inRange (mkRange @Range lower upper) value === (lower <= value && value <= upper)
      inRange (mkRange @RangeIE lower upper) value === (lower <= value && value < upper)
      inRange (mkRange @RangeEI lower upper) value === (lower < value && value <= upper)
      inRange (mkRange @RangeEE lower upper) value === (lower < value && value < upper)
  , testProperty "inRange is isomorphic to inRange_" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let checkIso :: (NiceRange range lower upper Integer, MonadTest m) => range Integer -> m ()
          checkIso range = inRange range value === (inRange_ -$ range ::: value)
      checkIso (mkRange @Range lower upper)
      checkIso (mkRange @RangeIE lower upper)
      checkIso (mkRange @RangeEI lower upper)
      checkIso (mkRange @RangeEE lower upper)
  , testProperty "inRangeCmp shows whether value underflows or overflows" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      inRangeCmp (mkRange @Range lower upper) value ===
        case (Prelude.compare value lower, Prelude.compare value upper) of
          (LT, _) -> LT
          (_, GT) -> GT
          _ -> EQ
      inRangeCmp (mkRange @RangeIE lower upper) value ===
        case (Prelude.compare value lower, Prelude.compare value upper) of
          (LT, _) -> LT
          (_, GT) -> GT
          (_, EQ) -> GT
          _ -> EQ
      inRangeCmp (mkRange @RangeEI lower upper) value ===
        case (Prelude.compare value lower, Prelude.compare value upper) of
          (LT, _) -> LT
          (EQ, _) -> LT
          (_, GT) -> GT
          _ -> EQ
      inRangeCmp (mkRange @RangeEE lower upper) value ===
        case (Prelude.compare value lower, Prelude.compare value upper) of
          (LT, _) -> LT
          (EQ, _) -> LT
          (_, GT) -> GT
          (_, EQ) -> GT
          _ -> EQ
  , testProperty "inRangeCmp is isomorphic to inRangeCmp_" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let checkIso :: (NiceRange range lower upper Integer, MonadTest m) => range Integer -> m ()
          checkIso range = ordToInt (inRangeCmp range value) === (inRangeCmp_ -$ range ::: value)
          ordToInt :: Ordering -> Integer
          ordToInt = \case
            EQ -> 0
            LT -> -1
            GT -> 1
      checkIso (mkRange @Range lower upper)
      checkIso (mkRange @RangeIE lower upper)
      checkIso (mkRange @RangeEI lower upper)
      checkIso (mkRange @RangeEE lower upper)
  , testProperty "isRangeEmpty checks if range is empty" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      isRangeEmpty (mkRange @Range lower upper)   === (lower > upper)
      isRangeEmpty (mkRange @RangeIE lower upper) === (lower >= upper)
      isRangeEmpty (mkRange @RangeEI lower upper) === (lower >= upper)
  , testProperty "isRangeEmpty is isomorphic to isRangeEmpty_" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let checkIso
            :: (CanCheckEmpty lower upper, NiceRange range lower upper Integer, MonadTest m)
            => range Integer -> m ()
          checkIso range = isRangeEmpty range === (isRangeEmpty_ -$ range)
      checkIso (mkRange @Range lower upper)
      checkIso (mkRange @RangeIE lower upper)
      checkIso (mkRange @RangeEI lower upper)
  , testProperty "assertRangeNonEmpty_ throws IFF range is empty" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let check
            :: forall range lower upper m.
              ( CanCheckEmpty lower upper, NiceRange range lower upper Integer, MonadTest m
              , Eq (range Integer), Show (range Integer))
            => range Integer -> m ()
          check range
            | isRangeEmpty range = first mfwsFailed (assertRangeNonEmpty_ err -$? range)
                === Left (MichelsonFailedWith (toVal range))
            | otherwise = (assertRangeNonEmpty_ err -$? range) === Right range
            where err :: range Integer : s :-> any
                  err = failWith
      check (mkRange @Range lower upper)
      check (mkRange @RangeIE lower upper)
      check (mkRange @RangeEI lower upper)
  ]

testCommon
  :: forall range lower upper.
     ( NiceRange range lower upper Integer
     , Eq (range Integer), Show (range Integer)
     )
  => [TestTree]
testCommon =
  [ testProperty "mkRange creates ranges with (rangeLower, rangeUpper)" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRange @range lower upper
      rangeLower range === lower
      rangeUpper range === upper
  , testProperty "mkRange is isomorphic to mkRange_" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      mkRange @range lower upper === (mkRange_ -$ (#min :! lower) ::: (#max :! upper))
  , testProperty "mkRangeFor creates ranges with (start, start + len)" $ property do
      start :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      len :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRangeFor @range start len
      rangeLower range === start
      rangeUpper range === start + len
  , testProperty "mkRangeFor is isomorphic to mkRangeFor_" $ property do
      start :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      len :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      mkRangeFor @range start len === (mkRangeFor_ -$ (#start :! start) ::: (#length :! len))
  , testProperty "rangeLower is isomorphic to rangeLower_" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRange @range lower upper
      rangeLower range === (rangeLower_ -$ range)
  , testProperty "rangeUpper is isomorphic to rangeUpper_" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRange @range lower upper
      rangeUpper range === (rangeUpper_ -$ range)
  , testProperty "assertInRange_ throws when value is out of range" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRange @range lower upper
          e = OnRangeAssertFailure
            { orafUnderflow = Lorentz.drop # failUsing [mt|underflow|]
            , orafOverflow = Lorentz.drop # failUsing [mt|overflow|]
            }
      first mfwsFailed (assertInRange_ e -$? range ::: value ::: ()) === case inRange range value of
        True -> Right ()
        False -> Left $ MichelsonFailedWith $ toVal $
          if (if isInclusive @lower then value < lower else value <= lower)
          then [mt|underflow|]
          else [mt|overflow|]
  , testProperty "assertInRange_ throws with mkOnRangeAssertFailureSimple" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRange @range lower upper
          e = mkOnRangeAssertFailureSimple $ failUsing [mt|range error|]
      first mfwsFailed (assertInRange_ e -$? range ::: value ::: ()) === case inRange range value of
        True -> Right ()
        False -> Left $ MichelsonFailedWith $ toVal [mt|range error|]
  , testProperty "assertInRangeSimple_ throws IFF value out of range" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRange @range lower upper
          e = failUsing [mt|range error|]
      first mfwsFailed (assertInRangeSimple_ e -$? range ::: value ::: ()) ===
        case inRange range value of
          True -> Right ()
          False -> Left $ MichelsonFailedWith $ toVal [mt|range error|]
  , testProperty "assertInRange_ throws with customErrorORAF" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRange @range lower upper
          e = customErrorORAF ! #underflow #underflowInt ! #overflow #overflowInt
      first mfwsFailed (assertInRange_ e -$? range ::: value ::: ()) === case inRange range value of
        True -> Right ()
        False -> Left $ MichelsonFailedWith $ toVal $
          if (if isInclusive @lower then value < lower else value <= lower)
          then ([mt|UnderflowInt|], RangeFailureInfo (isInclusive @lower) lower value)
          else ([mt|OverflowInt|], RangeFailureInfo (isInclusive @upper) upper value)
  , testProperty "assertInRange_ throws with customErrorORAFSimple" $ property do
      lower :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      upper :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      value :: Integer <- forAll $ Gen.integral $ Range.linearFrom 0 -100_000 100_000
      let range = mkRange @range lower upper
          e = customErrorORAFSimple #rangeError
      first mfwsFailed (assertInRange_ e -$? range ::: value ::: ()) === case inRange range value of
        True -> Right ()
        False -> Left $ MichelsonFailedWith $ toVal $
          if (if isInclusive @lower then value < lower else value <= lower)
          then ([mt|RangeError|], RangeFailureInfo (isInclusive @lower) lower value)
          else ([mt|RangeError|], RangeFailureInfo (isInclusive @upper) upper value)
  ]

type RFII = RangeFailureInfo Integer

[errorDocArg| "overflowInt" exception "Integer range overflow" RFII |]
[errorDocArg| "underflowInt" exception "Integer range underflow" RFII |]
[errorDocArg| "rangeError" exception "Integer range error" RFII |]
