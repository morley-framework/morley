-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Tests on ordering of documentation items.
module Test.Lorentz.Doc.ErrorArg
  ( unit_NoErrorArg
  , unit_UnitErrorArg
  , unit_UnitErrorArgUnit
  , unit_NoErrorArgBadArg
  , unit_NoErrorArgInternal
  , unit_NoErrorArgUnknown
  , unit_IntegerErrorArg
  ) where

import Language.Haskell.TH.Lib qualified as TH
import Language.Haskell.TH.Quote (quoteDec)
import Language.Haskell.TH.Syntax qualified as TH
import Test.Tasty.HUnit (Assertion, (@?=))

import Lorentz (CustomErrorHasDoc(..), ErrorArg, ErrorClass(..), NoErrorArg, UnitErrorArg)

import Lorentz.Util.TH
import Morley.Util.Interpolate

[errorDocArg|"errorName1" exception "An error happened"|]
[errorDocArg|"errorName2" bad-argument "An error happened"|]
[errorDocArg|"errorName3" contract-internal "An error happened"|]
[errorDocArg|"errorName4" unknown "An error happened"|]
[errorDocArg|"errorName5" exception "An error happened" UnitErrorArg|]
[errorDocArg|"errorName6" exception "An error happened" ()|]
[errorDocArg|"errorName7" exception "An error happened" Integer|]

unit_NoErrorArg :: Assertion
unit_NoErrorArg = do
  $(quoteDec errorDocArg [i|"errorName" exception "An error happened"|] >>= TH.lift) `shouldCompileTo`
    [d|
      type instance ErrorArg "errorName" = NoErrorArg
      instance CustomErrorHasDoc "errorName"
          where customErrClass = ErrClassActionException
                customErrDocMdCause = "An error happened"
    |]

unit_NoErrorArgBadArg :: Assertion
unit_NoErrorArgBadArg = do
  $(quoteDec errorDocArg [i|"errorName" bad-argument "An error happened"|] >>= TH.lift) `shouldCompileTo`
    [d|
      type instance ErrorArg "errorName" = NoErrorArg
      instance CustomErrorHasDoc "errorName"
          where customErrClass = ErrClassBadArgument
                customErrDocMdCause = "An error happened"
    |]

unit_NoErrorArgInternal :: Assertion
unit_NoErrorArgInternal = do
  $(quoteDec errorDocArg [i|"errorName" contract-internal "An error happened"|] >>= TH.lift) `shouldCompileTo`
    [d|
      type instance ErrorArg "errorName" = NoErrorArg
      instance CustomErrorHasDoc "errorName"
          where customErrClass = ErrClassContractInternal
                customErrDocMdCause = "An error happened"
    |]

unit_NoErrorArgUnknown :: Assertion
unit_NoErrorArgUnknown = do
  $(quoteDec errorDocArg [i|"errorName" unknown "An error happened"|] >>= TH.lift) `shouldCompileTo`
    [d|
      type instance ErrorArg "errorName" = NoErrorArg
      instance CustomErrorHasDoc "errorName"
          where customErrClass = ErrClassUnknown
                customErrDocMdCause = "An error happened"
    |]

unit_UnitErrorArg :: Assertion
unit_UnitErrorArg = do
  $(quoteDec errorDocArg [i|"errorName" exception "An error happened" UnitErrorArg|] >>= TH.lift) `shouldCompileTo`
    [d|
      type instance ErrorArg "errorName" = $(TH.conT $ TH.mkName "UnitErrorArg")
      instance CustomErrorHasDoc "errorName"
          where customErrClass = ErrClassActionException
                customErrDocMdCause = "An error happened"
    |]

unit_UnitErrorArgUnit :: Assertion
unit_UnitErrorArgUnit = do
  $(quoteDec errorDocArg [i|"errorName" exception "An error happened" ()|] >>= TH.lift) `shouldCompileTo`
    [d|
      type instance ErrorArg "errorName" = $(TH.conT $ TH.mkName "()")
      instance CustomErrorHasDoc "errorName"
          where customErrClass = ErrClassActionException
                customErrDocMdCause = "An error happened"
    |]

unit_IntegerErrorArg :: Assertion
unit_IntegerErrorArg = do
  $(quoteDec errorDocArg [i|"errorName" exception "An error happened" Integer|] >>= TH.lift) `shouldCompileTo`
    [d|
      type instance ErrorArg "errorName" = $(TH.conT $ TH.mkName "Integer")
      instance CustomErrorHasDoc "errorName"
          where customErrClass = ErrClassActionException
                customErrDocMdCause = "An error happened"
    |]

shouldCompileTo :: HasCallStack => [TH.Dec] -> TH.Q [TH.Dec] -> Assertion
shouldCompileTo actualDecs expectedQ = do
  expectedDecs <- TH.runQ expectedQ
  actualDecs @?= expectedDecs
