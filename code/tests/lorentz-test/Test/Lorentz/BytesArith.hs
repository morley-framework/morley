-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for bytes arithmetic
module Test.Lorentz.BytesArith
  ( test_Bytes_arith
  ) where

import Fmt (Buildable(..), hexF, pretty, unwordsF, (+|), (|+))
import Test.Tasty (TestTree)

import Lorentz ((#))
import Lorentz qualified as L
import Morley.Michelson.Typed (toVal, untypeValue)
import Test.Cleveland

data BytesOp
  = And ByteString ByteString ByteString
  | Or ByteString ByteString ByteString
  | Xor ByteString ByteString ByteString
  | Not ByteString ByteString
  | Lsr ByteString Natural ByteString
  | Lsl ByteString Natural ByteString

instance Buildable BytesOp where
  build = \case
    And x y r -> "And " +| unwordsF [hexF' x, hexF' y] |+ " = " +| hexF' r
    Or x y r  -> "Or "  +| unwordsF [hexF' x, hexF' y] |+ " = " +| hexF' r
    Xor x y r -> "Xor " +| unwordsF [hexF' x, hexF' y] |+ " = " +| hexF' r
    Not x r   -> "Not " +| hexF' x |+ " = " +| hexF' r
    Lsl x s r -> "Lsl " +| unwordsF [hexF' x, build s] |+ " = " +| hexF' r
    Lsr x s r -> "Lsr " +| unwordsF [hexF' x, build s] |+ " = " +| hexF' r
    where
      hexF' = ("0x" <>) . hexF

test_Bytes_arith :: [TestTree]
test_Bytes_arith = genTest <$>
  [ And "\x01\x02\x04" "\xFE\xFD\xFB" "\x00\x00\x00"
  , And "\x01\x02\x04" "\xFF\xFF\xFF" "\x01\x02\x04"
  , And "\x01\x02\x04" "\xFF\x00\xFF" "\x01\x00\x04"
  -- different lengths are matched end-wise, longer is truncated
  , And "\x00\x00\x00\xFF\xFF\xFF" "\x01\x02\x04" "\x01\x02\x04"

  , Or  "\x01\x02\x04" "\xFE\xFD\xFB" "\xFF\xFF\xFF"
  , Or  "\x01\x02\x04" "\x00\x00\x00" "\x01\x02\x04"
  , Or  "\x01\x02\x04" "\xFF\x00\xFF" "\xFF\x02\xFF"
  -- different lengths are matched end-wise, shorter is left-padded
  , Or "\xFF\xFF\xFF\x00\x00\x00" "\x01\x02\x04" "\xFF\xFF\xFF\x01\x02\x04"

  , Xor "\x01\x02\x04" "\xFE\xFD\xFB" "\xFF\xFF\xFF"
  , Xor "\x01\x02\x04" "\x00\x00\x00" "\x01\x02\x04"
  , Xor "\x01\x02\x04" "\xFF\x02\xFF" "\xFE\x00\xFB"
  -- different lengths are matched end-wise, shorter is left-padded
  , Xor "\xFF\xFF\xFF\x00\x00\x00" "\x01\x02\x04" "\xFF\xFF\xFF\x01\x02\x04"

  , Not "\x01\x02\x04" "\xFE\xFD\xFB"

  -- ``0x1234 LSL 1`` is ``0x002468``, according to the docs
  , Lsl "\x12\x34" 1 "\x00\x24\x68"

  -- ``0x123499 LSR 9`` is ``0x091a``, according to the docs
  , Lsr "\x12\x34\x99" 9 "\x09\x1a"

  -- 0x06 LSR 1 = 0x03
  , Lsr "\x06" 1 "\x03"

  -- 0x06 LSR 8 = 0x (empty bytes)
  , Lsr "\x06" 8 ""

  -- 0x0006 LSR 1 = 0x0003  (not 0x03)
  , Lsr "\x00\x06" 1 "\x00\x03"

  -- 0x0006 LSR 8 = 0x00
  , Lsr "\x00\x06" 8 "\x00"

  -- 0x001234 LSR 0 = 0x001234
  , Lsr "\x00\x12\x34" 0 "\x00\x12\x34"

  -- 0x001234 LSR 30 = 0x
  , Lsr "\x00\x12\x34" 30 ""

  -- very large shift works
  , Lsr "\x00\x12\x34" (fromIntegralOverflowing (maxBound @Int)) ""
  , Lsr "\x00\x12\x34" (fromIntegralOverflowing (maxBound @Int) + 1) ""
  , Lsr "\x00\x12\x34" (fromIntegralOverflowing (maxBound @Int) * 10) ""
  ]

genTest :: BytesOp -> TestTree
genTest op = testScenario (pretty op) $ scenario do
  mkRC op

mkRC :: MonadCleveland caps m => BytesOp -> m ()
mkRC = \case
  And x y r ->
    runCode RunCode
      { rcContract = L.defaultContract @ByteString @ByteString $ L.unpair # L.and # L.nil # L.pair
      , rcStorage = untypeValue $ toVal x
      , rcParameter = untypeValue $ toVal y
      , ..
      } @@== r
  Or x y r ->
    runCode RunCode
      { rcContract = L.defaultContract @ByteString @ByteString $ L.unpair # L.or # L.nil # L.pair
      , rcStorage = untypeValue $ toVal x
      , rcParameter = untypeValue $ toVal y
      , ..
      } @@== r
  Xor x y r ->
    runCode RunCode
      { rcContract = L.defaultContract @ByteString @ByteString $ L.unpair # L.xor # L.nil # L.pair
      , rcStorage = untypeValue $ toVal x
      , rcParameter = untypeValue $ toVal y
      , ..
      } @@== r
  Not x r ->
    runCode RunCode
      { rcContract = L.defaultContract @() @ByteString $ L.cdr # L.not # L.nil # L.pair
      , rcStorage = untypeValue $ toVal x
      , rcParameter = untypeValue $ toVal ()
      , ..
      } @@== r
  Lsl x s r ->
    runCode RunCode
      { rcContract = L.defaultContract @Natural @ByteString $ L.unpair # L.swap # L.lsl # L.nil # L.pair
      , rcStorage = untypeValue $ toVal x
      , rcParameter = untypeValue $ toVal s
      , ..
      } @@== r
  Lsr x s r ->
    runCode RunCode
      { rcContract = L.defaultContract @Natural @ByteString $ L.unpair # L.swap # L.lsr # L.nil # L.pair
      , rcStorage = untypeValue $ toVal x
      , rcParameter = untypeValue $ toVal s
      , ..
      } @@== r
  where
    rcAmount = 0
    rcBalance = 0
    rcSource = Nothing
    rcNow = Nothing
    rcLevel = Nothing
