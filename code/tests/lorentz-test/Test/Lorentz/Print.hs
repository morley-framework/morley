-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on Lorentz contracts pretty-printing.
module Test.Lorentz.Print
  ( test_Print_parameter_annotations
  , test_Print_lambda
  ) where

import Fmt (prettyText)
import Lorentz hiding (contract, unpack)
import Prelude hiding (drop, swap)

import Test.HUnit ((@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Morley.Michelson.Typed hiding (Contract, ContractCode, defaultContract)
import Morley.Michelson.Untyped (contractParameter)

data MyEntrypoints1
  = Do1 Integer
  | Do2 (Integer, Integer)
  | Do3
  deriving stock Generic
  deriving anyclass IsoValue

instance ParameterHasEntrypoints MyEntrypoints1 where
  type ParameterEntrypointsDerivation MyEntrypoints1 = EpdPlain

contract :: Contract MyEntrypoints1 () ()
contract = defaultContract $
  drop # unit # nil # pair

test_Print_parameter_annotations :: [TestTree]
test_Print_parameter_annotations =
  [ testCase "Simple parameter" $
      let typedContract = toMichelsonContract contract
          untypedContract = convertContract typedContract
      in prettyText (contractParameter untypedContract)
         @?=
         "or (int %do1) (or (pair %do2 int int) (unit %do3))"
  ]

test_Print_lambda :: [TestTree]
test_Print_lambda =
  [ testCase "Prints correct lambda instruction" do
      let
        code :: Lambda Integer (Lambda Integer ())
        code = mkLambda $ drop # lambda (drop # unit)
      printLorentzValue True code @?=
        "{ DROP; LAMBDA int unit { DROP; UNIT } }"
  , testCase "Prints correct Lambda_rec instruction" do
      let
        code :: Lambda Integer (Lambda Integer ())
        code = mkLambdaRec $ dropN @2 # lambdaRec (dropN @2 # unit)
      printLorentzValue True code @?=
        "Lambda_rec { DROP 2; LAMBDA_REC int unit { DROP 2; UNIT } }"
  ]

data TestParam
  = TestCon1 ("a" :! Natural, "b" :! Natural)
  | TestCon2 Bool
  deriving stock Generic
  deriving anyclass IsoValue

instance ParameterHasEntrypoints TestParam where
  type ParameterEntrypointsDerivation TestParam = EpdRecursive
