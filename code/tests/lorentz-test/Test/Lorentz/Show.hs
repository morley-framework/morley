-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.Show
  ( unit_ConstrainedSomeShowToplevel
  , unit_ConstrainedSomeShowSubexpression
  , unit_NFixedShow
  ) where

import Debug qualified

import Test.HUnit (Assertion, assertEqual)

import Lorentz (ConstrainedSome(..), NFixed(..))

-- * ConstrainedSome Show

constrainedSome :: ConstrainedSome Show
constrainedSome = ConstrainedSome ()

unit_ConstrainedSomeShowToplevel :: Assertion
unit_ConstrainedSomeShowToplevel =
  assertEqual @Text "Top-level `ConstrainedSome Show` is `show`n without parens"
    "ConstrainedSome ()"
    (Debug.show constrainedSome)


unit_ConstrainedSomeShowSubexpression :: Assertion
unit_ConstrainedSomeShowSubexpression =
  assertEqual @Text "Subexpression `ConstrainedSome Show` is parenthesized"
    "Just (ConstrainedSome ())"
    (Debug.show (Just constrainedSome))


-- * NFixed p

nFixed :: NFixed 1
nFixed = MkNFixed 17.0

unit_NFixedShow :: Assertion
unit_NFixedShow =
  assertEqual @Text "`NFixed`'s `Show` is newtype-ish"
    "17.0"
    (Debug.show nFixed)
