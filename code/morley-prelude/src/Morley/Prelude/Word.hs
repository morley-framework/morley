-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE NoImplicitPrelude, CPP #-}

module Morley.Prelude.Word
  ( Word63
  , Word62
  )
  where

import Data.Bits (Bits, FiniteBits)
import Universum (Bounded, Enum, Eq, Integral, Num, Ord, Read, Real, Show)

import Data.IntCast (IntBaseType, IntBaseTypeK(..))
import Data.Word.Odd qualified as OddWord

newtype Word62 = Word62 OddWord.Word62
  deriving newtype (Eq, Ord, Show, Read, Num, Real, Bounded, Enum, Integral, Bits, FiniteBits)
newtype Word63 = Word63 OddWord.Word63
  deriving newtype (Eq, Ord, Show, Read, Num, Real, Bounded, Enum, Integral, Bits, FiniteBits)

type instance IntBaseType Word63 = 'FixedWordTag 63
type instance IntBaseType Word62 = 'FixedWordTag 62
