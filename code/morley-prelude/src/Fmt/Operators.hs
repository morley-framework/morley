-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE NoImplicitPrelude #-}

-- | Extra operators on top of "Prettyprinter" with additional semantics.
module Fmt.Operators
  ( (<//>)
  , (</>)
  , (<+>)
  , (<$>)
  , (<$$>)
  ) where

import Universum ((<>))

import Fmt.Utils (Doc, linebreak, softbreak)
import Prettyprinter (line, softline, space)
import Prettyprinter.Internal (pattern Empty)

(<+>), (<$>), (</>), (<$$>), (<//>) :: Doc -> Doc -> Doc
Empty <+> y = y
x <+> Empty = x
x <+> y = x <> space <> y
infixr 6 <+>

x <$> Empty = x
Empty <$> y = y
x <$> y = x <> line <> y

x </> Empty = x
Empty </> y = y
x </> y = x <> softline <> y

x <$$> Empty = x
Empty <$$> y = y
x <$$> y = x <> linebreak <> y

x <//> Empty = x
Empty <//> y = y
x <//> y = x <> softbreak <> y
infixr 5 </>,<//>,<$>,<$$>
