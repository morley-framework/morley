-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Utility functions to read sample contracts (for testing).

module TestSuite.Util.Contracts
  ( contractsDir
  , inContractsDir
  , (</>)
  ) where

import System.FilePath ((</>))

-- | Directory with sample contracts.
contractsDir :: FilePath
contractsDir = "../../contracts/"

inContractsDir :: FilePath -> FilePath
inContractsDir = (contractsDir </>)
