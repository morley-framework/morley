-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.BatchOperation
  ( test_SomeCases
  ) where

import Test.Tasty (TestTree)

import Morley.Tezos.Core (mkMutez)
import Morley.Util.SizedList qualified as SL
import Test.Cleveland
import Test.Cleveland.Lorentz.Consumer

unsafeMkMutez' :: Natural -> Mutez
unsafeMkMutez' = either error id . mkMutez

test_SomeCases :: [TestTree]
test_SomeCases =
  [ testScenario "Origination and transfer within a batch work" $ scenario do
      test1 <- newFreshAddress auto

      contract1 <- inBatch $ do
        contract1 <- originate "c" ([] :: [()]) contractConsumer
        transfer test1 [tz|100u|]
        return contract1

      getBalance test1 @@== 100

      transfer contract1 [tz|200u|]
      getBalance contract1 @@== 200


  , testScenario "Loops within batch work" $ scenario do
      addresses <- traverse newFreshAddress $ SL.replicateT @5 auto
      let balances = SL.generate $ \i -> unsafeMkMutez' $ (i + 1) * 100

      inBatch $ for_ (SL.zip addresses balances) $ uncurry transfer

      traverse getBalance addresses @@== balances


  , testScenario "Can return multiple values from a batch" $ scenario do
      (_contract1, _contract2) <- inBatch $ do
        contract1 <- originate "c1" ([] :: [Integer]) contractConsumer
        contract2 <- originate "c2" ([] :: [Integer]) contractConsumer
        return (contract1, contract2)

      return ()
  ]
