-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Utility functions for checking predicates on contract's entrypoints for
-- Lorentz
module Test.Cleveland.Lorentz.Entrypoints
  ( -- * Entrypoint spec passed on the type level
    testContractCoversEntrypointsT
  , testContractMatchesEntrypointsT

    -- * Entrypoint spec passed as a value
  , testContractCoversEntrypoints
  , testContractMatchesEntrypoints
  ) where

import Test.Tasty (TestName)

import Lorentz (NiceParameterFull)
import Test.Cleveland.Lorentz.Internal.Entrypoints
import Test.Cleveland.Michelson.Internal.Entrypoints (ignoreExtraEntrypoints)

-- | Expect the contract to cover or exactly match with the entrypoints given in spec passed as the
-- first type argument. Checks both the contract type and the contract itself (when represented as
-- an untyped Michelson contract).
testContractCoversEntrypointsT,
  testContractMatchesEntrypointsT :: forall exp. TestName -> ContractEPTypeTest exp
testContractCoversEntrypointsT = testCompareContractEntrypointsT @exp ignoreExtraEntrypoints
testContractMatchesEntrypointsT = testCompareContractEntrypointsT @exp id

-- | Expect the contract to cover or exactly match with the entrypoints given in spec passed as a
-- 'Map' of names to types. Checks both the contract type and the contract itself (when represented as
-- an untyped Michelson contract).
testContractCoversEntrypoints,
  testContractMatchesEntrypoints :: NiceParameterFull contractEps => TestName -> ContractEPTest contractEps st vd
testContractCoversEntrypoints = testCompareContractEntrypoints ignoreExtraEntrypoints
testContractMatchesEntrypoints = testCompareContractEntrypoints id
