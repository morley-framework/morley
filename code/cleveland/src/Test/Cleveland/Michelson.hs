-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Module containing some utilities for testing Michelson contracts using
-- Haskell testing frameworks.
-- It's Morley testing EDSL.
-- We focus on @tasty@ and @hedgehog@ because that's what we mostly use in
-- our tests, but we also provide helpers for @hspec@, and defining helpers
-- for other libraries (e. g. @QuickCheck@) shouldn't be hard.
-- We don't provide helpers for other libraries to have less dependencies.

module Test.Cleveland.Michelson
  ( -- * Importing a contract
    testTreesWithContract
  , testTreesWithUntypedContract
  , testTreesWithTypedContract
  , concatTestTrees
  , importContract
  , importSomeContract
  , importUntypedContract

  -- * Unit testing
  , testContractCoversEntrypoints
  , testContractMatchesEntrypoints

  -- * General utilities
  , failedTest
  , succeededTest
  , eitherIsLeft
  , eitherIsRight
  , meanTimeUpperBoundProp
  , meanTimeUpperBoundPropNF

  -- * Re-exports
  --
  -- | These functions from @Time@ are re-exported here to make it convenient to call
  -- 'meanTimeUpperBoundProp' and 'meanTimeUpperBoundPropNF'.
  , mcs, ms, sec, minute

  -- * Autodoc testing
  , runDocTests
  , testDocBasic
  , excludeDocTests
  ) where

import Test.Cleveland.Doc.Michelson
import Test.Cleveland.Michelson.Entrypoints
import Test.Cleveland.Michelson.Import
import Test.Cleveland.Util
