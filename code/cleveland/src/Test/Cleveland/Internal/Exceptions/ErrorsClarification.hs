-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | This module defines 'ErrorsClarification' and required instances to use it
-- with 'ExceptionAnnotation'.
module Test.Cleveland.Internal.Exceptions.ErrorsClarification
  ( module Test.Cleveland.Internal.Exceptions.ErrorsClarification
  ) where

import Fmt (Doc, nameF)

import Test.Cleveland.Internal.Exceptions.Annotated

-- | Used to add text prefixes to exception messages.
--
-- Implementation detail of 'Test.Cleveland.clarifyErrors'.
newtype ErrorsClarification = ErrorsClarification [Doc]
  deriving stock Show
  deriving newtype Semigroup

instance ExceptionAnnotation ErrorsClarification where
  displayAnnotation (ErrorsClarification as) = flip (foldr nameF) as
