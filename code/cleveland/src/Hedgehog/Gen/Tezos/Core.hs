-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Tezos.Core
  ( genChainId
  , genMutez
  , genTimestamp
  ) where

import Hedgehog (MonadGen, Range)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Tezos.Core
  (ChainId(..), Mutez(..), Timestamp, mkMutez, timestampFromSeconds, timestampToSeconds)

import Hedgehog.Range.Defaults ()

genChainId :: MonadGen m => m ChainId
genChainId = UnsafeChainId <$> Gen.bytes (Range.singleton 4)

-- | Generates an arbitrary `Mutez` value constrained to the given range.
genMutez :: MonadGen m => Range Mutez -> m Mutez
genMutez range = unsafe . mkMutez <$> Gen.word64 (fromIntegral . unMutez <$> range)

genTimestamp :: MonadGen m => Range Timestamp -> m Timestamp
genTimestamp range =
  timestampFromSeconds <$> Gen.integral (timestampToSeconds <$> range)
