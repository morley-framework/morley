-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Tezos.Crypto
  ( genPublicKey
  , genSecretKey
  , genSignature
  , genKeyHashTag
  , genKeyHash
  , genPublicKey'
  , genSecretKey'
  , genKeyHash'
  , genKeyType
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Tezos.Crypto

import Hedgehog.Gen.Tezos.Crypto.BLS qualified as BLS
import Hedgehog.Gen.Tezos.Crypto.Ed25519 qualified as Ed25519
import Hedgehog.Gen.Tezos.Crypto.P256 qualified as P256
import Hedgehog.Gen.Tezos.Crypto.Secp256k1 qualified as Secp256k1

genKeyType :: MonadGen m => m KeyType
genKeyType = Gen.choice $ pure <$> [minBound..]

genPublicKey :: MonadGen m => m PublicKey
genPublicKey = genPublicKey' genKeyType

genPublicKey' :: MonadGen m => m KeyType -> m PublicKey
genPublicKey' keyType = toPublic <$> genSecretKey' keyType

genSecretKey :: MonadGen m => m SecretKey
genSecretKey = genSecretKey' genKeyType

genSecretKey' :: MonadGen m => m KeyType -> m SecretKey
genSecretKey' keyType = detSecretKey' <$> keyType <*> Gen.bytes (Range.singleton 32)

genSignature :: MonadGen m => m Signature
genSignature = Gen.choice
  [ SignatureEd25519 <$> Ed25519.genSignature
  , SignatureSecp256k1 <$> Secp256k1.genSignature
  , SignatureP256 <$> P256.genSignature
  , SignatureBLS <$> BLS.genSignature
  , SignatureGeneric <$> Gen.bytes (Range.singleton signatureLengthBytes)
  ]

genKeyHashTag :: MonadGen m => m KeyHashTag
genKeyHashTag = Gen.element $ toList allHashTags

genKeyHash :: MonadGen m => m KeyHash
genKeyHash = genKeyHash' genKeyType

genKeyHash' :: MonadGen m => m KeyType -> m KeyHash
genKeyHash' keyType = hashKey <$> genPublicKey' keyType
