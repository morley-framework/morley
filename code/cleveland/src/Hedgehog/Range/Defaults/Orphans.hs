-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | One place for orphan instances
module Hedgehog.Range.Defaults.Orphans
  () where

import Data.Default (Default(..))
import Hedgehog.Range (Range)
import Hedgehog.Range qualified as Range

import Morley.Michelson.ErrorPos (Pos(..))
import Morley.Michelson.Typed (T(..), Value'(..))
import Morley.Michelson.Untyped (StackRef(..))
import Morley.Tezos.Core (Mutez(..), Timestamp, timestampFromSeconds, timestampToSeconds)

import Hedgehog.Range.Tezos.Core.Timestamp
import Test.Cleveland.Instances ()

instance Default (Range Pos) where
  def = Pos <$> Range.linearBounded

instance Default (Range StackRef) where
  def = StackRef <$> Range.linear 0 (fromIntegral $ maxBound @Word64)

instance Default (Range Mutez) where
  def = Range.linearBounded

instance Default (Range Timestamp) where
  def = timestampFromSeconds <$>
    Range.linear (timestampToSeconds minTimestamp) (timestampToSeconds maxTimestamp)

instance Default (Range (Value' instr 'TInt)) where
  def = VInt <$> Range.linearFrom 0 -1000 1000

instance Default (Range (Value' instr 'TNat)) where
  def = VNat <$> Range.linearFrom 0 0 1000
