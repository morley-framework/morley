# SPDX-FileCopyrightText: 2021 Tocqueville Group
# SPDX-License-Identifier: LicenseRef-MIT-TQ

BEGIN {
  err=0;
  errs="";
  outp="";
  # invalid haddock warnings are interleaved independently of the rest of output,
  # hence we handle those separately.
  invalid_haddock=0;
  invalid_haddocks="";
  library="";
  hint="";
  indent = "    ";
  indent2 = indent indent;
  indent3 = indent2 indent;
}

/^Running Haddock on library for/ {
  gsub(/^Running Haddock on library for /,"");
  gsub(/\.*$/,"");
  library=$0;
}

/^Warning:/ ||
/Missing documentation for:/ ||
/warning: \[-Winvalid-haddock\]$/ ||
/^$/ ||
/^ *[0-9]+% *\( *[0-9]+ *\/ *[0-9]+\) in '.+'$/ {
  if (err=1 && hint!="") {
    errs= errs "\n" hint;
  }
  err=0;
  invalid_haddock=0;
  hint="";
}

/^Warning: '.+' is out of scope\./ ||
/^Warning: '.+' is ambiguous\. It is defined/ {
  err=1;
  ident=$0;
  gsub(/^[^']*'/,"",ident);
  gsub(/'[^']*$/,"",ident);
  hint=indent2 "Hint: Haddock doesn't differentiate between ` and ' in most cases, \n"\
    indent3 "so check if the file also contains \n"\
    indent3 "`" ident "`, `" ident "' or '" ident "`, \n"\
    indent3 "those will also produce this warning.";
}

/warning: \[-Winvalid-haddock\]$/ {
  invalid_haddock=1;
}

/^Warning: '.+' is out of scope\./ {
  hint=hint "\n" \
    indent2 "Hint: Qualifying identifiers in Haddock is generally discouraged,\n"\
    indent3 "due to the silently broken links it can create.\n"\
    indent3 "Consider using @"ident"@ instead";
}

/^Warning: '.+' is ambiguous\. It is defined/ {
  hint=hint "\n"\
    indent2 "Hint: Use t'" ident "' or v'" ident "' to specify\n"\
    indent3 "whether it is a type or a value, respectively.";
}

/^ *[0-9]+% *\( *[0-9]+ *\/ *[0-9]+\) in '.+'$/ {
  gsub(/^ *[0-9]+% *\( *[0-9]+ *\/ *[0-9]+\) in '/, "");
  gsub(/'$/, "");
  if (errs != "") {
    if (outp != "")
      outp=outp "\n\n"
    outp=outp "In package " library " module " $0 ":" errs;
  }
  errs="";
}

{
  if (err)
    errs=errs "\n" indent $0;
  if (invalid_haddock)
    invalid_haddocks = invalid_haddocks "\n" $0;
}

/ *\| *\^+\.*$/ {
  invalid_haddock=0;
}

END {
  if (outp != "" || invalid_haddocks != "") {
    print "Fixable Haddock warnings found:\n" outp invalid_haddocks;
    exit 1;
  }
}
